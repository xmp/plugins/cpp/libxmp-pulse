#ifndef XMP_PULSE_OPERATOR_H
#define XMP_PULSE_OPERATOR_H

#include <pthread.h>

#include <xmp/plugin/cpp/OperatorBase.h>
#include <xmp/core/operator/OperatorParameters.h>
#include <xmp/plugin/cpp/LogMaker.h>

class PulseOperator : public xmp::plugin::OperatorBase
{
public:
    PulseOperator(
            std::string const &id,
            xmp::core::OperatorPriority priority,
            xmp::core::IOperatorParameters const *parameters,
            xmp::core::IContext *context);

    void execute();
    bool start();
    bool stop();

    void setContent(const std::string &content);
    void setTimeout(const unsigned int &timeoutMsec);

private:
    xmp::core::IOutput *out;

private:
    static void *pulseThread(void *data);

    std::string what;
    unsigned int timeout;

    pthread_t thread;
    bool isRunned;
};


#endif // XMP_PULSE_OPERATOR_H
