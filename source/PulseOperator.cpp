#include <unistd.h>

#include "PulseOperator.h"

PulseOperator::PulseOperator(
        const std::string &id,
        xmp::core::OperatorPriority priority,
        const xmp::core::IOperatorParameters *parameters,
        xmp::core::IContext *context)
    : xmp::plugin::OperatorBase(id, priority, context),
      timeout(0),
      thread(0),
      isRunned(false),
      out(NULL)
{
    bool hasError = false;
    if (char const *value = parameters->getParameter("message")) {
        what = value;
    }
    else {
        hasError = true;
        xmp::LogMaker::error(this) << "Pulse operator with id '" << id << "' missing 'message' attribute";
    }

    if (char const *value = parameters->getParameter("timeout")) {
        timeout = atoi(value);
        if (timeout == 0) {
            hasError = true;
            xmp::LogMaker::error(this) << "Pulse operator with id '" << id << "' has invalid 'timeout\' attribute";
        }
    }
    else {
        hasError = true;
        xmp::LogMaker::error(this) << "Pulse operator with id '" << id << "' missing 'timeout' attribute";

    }

    if (hasError) {
        xmp::LogMaker::fatal(this) << "Pulse operator with id '" << id << "' failed to initialize";
    }

    out = registerOutput("out");
}

void PulseOperator::execute()
{
    xmp::core::IBuffer *buffer = getContext()->createBuffer();
    for (std::size_t i = 0; i < what.size(); i++) {
        buffer->pushBack(what.at(i));
    }
    out->write(buffer);
}

bool PulseOperator::start()
{
    if (!isRunned) {
        isRunned = true;
        if (pthread_create(&thread, NULL, (void * (*)( void *)) &PulseOperator::pulseThread, this)) {
            xmp::LogMaker::fatal(this) << "Pulse operator with id '" << getId() << "' failed to create thread";
            return false;
        }
        return true;
    }
    return false;
}

bool PulseOperator::stop()
{
    if (isRunned) {
        isRunned = false;
        pthread_join(thread, NULL);
        return true;
    }
    return false;
}

void *PulseOperator::pulseThread(void *data)
{
    PulseOperator *o = static_cast<PulseOperator *>(data);
    while (o->isRunned) {
        usleep(o->timeout * 1000);
        o->getContext()->dispatch(o);
    }
    return NULL;
}
