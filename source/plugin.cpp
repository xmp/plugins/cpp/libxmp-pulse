#include <xmp/plugin/cpp/plugin.h>

#include <xmp/plugin/cpp/GenericOperatorCreator.h>

#include "PulseOperator.h"

void registerCreators(xmp::core::IOperatorStore *store)
{
    store->registerCreator("pulse", new xmp::plugin::GenericOperatorCreator<PulseOperator>("pulse"));
}

