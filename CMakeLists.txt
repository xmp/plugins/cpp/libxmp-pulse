cmake_minimum_required(VERSION 3.8)
project(libxmp-pulse)

add_subdirectory(lib/libxmp-plugin-core)

add_library(xmp-pulse SHARED
        source/plugin.cpp
        source/PulseOperator.cpp)

target_link_libraries(xmp-pulse PUBLIC xmp-plugin-core)
